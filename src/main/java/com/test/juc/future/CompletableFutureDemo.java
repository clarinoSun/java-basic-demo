package com.test.juc.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class CompletableFutureDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        test1();
//        test2();
//        test3();
//        test4();
        test5();
    }

    private static void test1() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        // 不指定线程池的话，使用默认的fork-join线程池来执行
        CompletableFuture<String> task1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("1: " + Thread.currentThread().getName());
            return "1: hello";
        }, executorService);

        System.out.println(task1.get());

        CompletableFuture<String> task2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("2: " + Thread.currentThread().getName());
            return "2: hello";
        });

        System.out.println(task2.get());
    }

    private static void test2() {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        CompletableFuture.runAsync(() -> {
            System.out.println(Thread.currentThread().getName());
        }, executorService);

    }

    private static void test3() {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Supplier<String> taskA = () -> {
            System.out.println("1:" + Thread.currentThread().getName());
            return "hello";
        };

        Function<String, String> taskB = s -> {
            System.out.println("2:" + Thread.currentThread().getName());
            return s + " world";
        };

        Consumer<String> taskC = System.out::println;

        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(taskA, executorService);
        // 不开启sleep,使用线程池执行; 开启sleep 1s后，taskA已执行完，将用主线程执行taskB
        // sleep(1000);
        CompletableFuture<String> future2 = future1.thenApply(taskB);
        future2.thenAccept(taskC);

        System.out.println("haha");
    }

    private static void test4() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        // 不指定线程池的话，使用默认的fork-join线程池来执行
        CompletableFuture<String> futureA = CompletableFuture.supplyAsync(() -> {
            System.out.println("1: " + Thread.currentThread().getName());
            return "hello";
        }, executorService);

        CompletableFuture<String> futureB = CompletableFuture.supplyAsync(() -> {
            System.out.println("2: " + Thread.currentThread().getName());
            return " world";
        });

        CompletableFuture<String> result = futureA.thenCombine(futureB, (resultA, resultB) -> {
            System.out.println(Thread.currentThread().getName());
            return resultA + resultB;
        });

        System.out.println(result.get());
    }

    private static void test5() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        // 不指定线程池的话，使用默认的fork-join线程池来执行
        CompletableFuture<String> futureA = CompletableFuture.supplyAsync(() -> {
            System.out.println("1: " + Thread.currentThread().getName());
            return "hello";
        }, executorService);

        CompletableFuture<String> futureB = CompletableFuture.supplyAsync(() -> {
            System.out.println("2: " + Thread.currentThread().getName());
            return " world";
        });

//        futureA.runAfterEither(futureB, () -> System.out.println("!!!"));
//        futureA.runAfterBoth(futureB, () -> System.out.println("!!!"));

        // 两个任务都执行完后，才执行下面的
//        CompletableFuture.allOf(futureA, futureB).join();
        CompletableFuture.anyOf(futureA, futureB).join();
        System.out.println("end....");
    }


    private static void sleep(int i) {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
